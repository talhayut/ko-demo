import * as mongoose from 'mongoose';


class UsersCollection {

    private userModel: any;
    private userSchema : any;

    public constructor(public mongol: any){
      let schema = mongoose.Schema;

      this.userSchema  = new schema({
            username: String,
            password: String
        });

      this.userModel = mongol.model('users', this.userSchema);
    }

    public checkUser = (username, password): Promise<string> => {
        return new Promise((resolve, reject) => {
            this.userModel.find({username: username, password: password}, {_id: 0, __v: 0}, (err, result) => {
                if (err) {
                    console.error(err);
                    reject(err.toString());
                }

                if (result[0] === 'undefined' || !result[0]) {
                    reject("User does not exist");
                }
                resolve('User exists!');
            });
        });
    };
    
    public createUserIfNotExist = (username, password): Promise<string> => {
        return new Promise(async (resolve, reject) => {
            try {
                await this.checkUser(username, password).then((res) => {
                    reject(res);
                }).catch((e) => {
                    console.log(e);
                    resolve(this.createUserInDb(username, password));
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    private createUserInDb = (username, password): Promise<string> => {
        return new Promise(async (resolve, reject) => {
            let user = new this.userModel({username: username, password: password});
            try {
                await user.save((err) => {
                    if (err) {
                        console.error(err);
                        reject(err.toString());
                    }
                    resolve(`Created user ${username} successfully!`);
                });
            } catch (e) {
                reject(e);
            }
        });
    }
}


export { UsersCollection }