import * as chai from 'chai';
import {Validator} from './Validator'

import * as chaiPromised from 'chai-as-promised';

chai.use(chaiPromised);
chai.should();

let validator;
let file = 'file.txt';
let content = 'hi';

describe('Tests for validator', () => {

    beforeEach(() => {
        validator = new Validator();
    });

    describe('Tests for content validation', () => {
        it('should return a bool', () => {
            validator.validateContent('').should.be.a('boolean');
        });

        it('should return true if content is valid', () => {
            validator.validateContent(content).should.be.true;
        });

        it('should return false if no content exists', () => {
            validator.validateContent('').should.be.false;
        });
    });

    describe('Tests for extension extraction', () => {
        it('should validate the file extension', () => {
            validator.extensionValidator(file, 'txt').should.be.true;
        });

        it('should fail for extensions other than .txt', () => {
            validator.extensionValidator(file, 'pdf').should.be.false;
        });

    });

});