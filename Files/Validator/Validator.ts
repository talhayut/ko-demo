import {extname} from "path";

class Validator {

    public constructor() {}

    public validateContent = (fileContent: string): boolean => {
        if (fileContent.length <= 0) {
            return false;
        } else {

            return true;
        }
    };

    public extensionValidator = (fileName: string, extensionType: string): boolean => {
        let ext = extname(fileName);
        if (ext === `.${extensionType}`) {
            return true;
        } else {
            return false;
        }
    };
}

export {Validator}