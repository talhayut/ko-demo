import * as fs from 'fs';
import {IFile} from './Objects/IFile';
import {FileValidator} from "./Validator/FileValidator"

class Files {

    private validator;

    public constructor() {
        this.validator = new FileValidator();
    }

    public createFile = (fileName, content): Promise<any> => {
        return new Promise(async (resolve, reject) => {
            try {
                await this.validator.validateFile(fileName, content);
                fs.writeFile(`./${fileName}`, content, err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(content);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    public deleteFile = (fileName): Promise<any> => {
        return new Promise((resolve, reject) => {
            fs.unlink(`./${fileName}`, err => {
                if (err) {
                    reject(err);
                }
                resolve();
            });
        });
    };

    public listFiles = (): Promise<Array<IFile>> => {
        return new Promise<Array<IFile>>((resolve, reject) => {
            fs.readdir('./', async (err, files) => {
                if (err) {
                    reject(err);
                } else {

                    let filesArray: Array<IFile> = [];

                    for (let file of files) {
                        try {
                            let stats = await this.getFile(file);
                            filesArray.push(stats[0]);
                        } catch (e) {
                            console.error(e);
                        }
                    }
                    resolve(filesArray);
                }
            });
        });
    };

    public getFile = (fileName): Promise<Array<IFile>> => {
        return new Promise<Array<IFile>>(async (resolve, reject) => {
            try {
                await this.validator.validateFile(fileName, "fakeContent");
                let fileContent = await this.readFile(fileName);

                fs.stat(`./${fileName}`, (err, stats) => {
                    if (err) {
                        reject(err);
                    }
                    let fileInfo: IFile = {
                        fileName: fileName,
                        createdAt: stats.birthtime,
                        sizeInBytes: stats.size,
                        content: fileContent
                    };
                    resolve([fileInfo]);
                });
            } catch (e) {
                reject(e);
            }
        });
    };

    public updateFile = (fileName, content): Promise<Array<string>> => {
        return new Promise<Array<string>>(async (resolve, reject) => {
            try {
                await this.validator.validateFile(fileName, content);
                fs.writeFile(`./${fileName}`, content, err => {
                    if (err) {
                        reject(err);
                    }
                    resolve(content);
                });
            } catch (e) {
                reject('Cannot update file with no content' + e);
            }
        });
    };
    
    private readFile = (fileName): Promise<string> => {
        return new Promise<string>(async (resolve, reject) => {
            try {
                await fs.readFile(`./${fileName}`, (err, data) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(data.toString());
                });
            } catch (e) {
                reject('Cannot get content' + e);
            }
        });
    }

}

export {Files}