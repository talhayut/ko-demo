Feature:
As a user of the app
I want to delete a file

    Scenario: Delete a file using existing name and new content
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "taltool.txt" }
        When I DELETE /v1/files
        Then response code should be 200
        Then response body should contain message

    Scenario: Delete a file using no file name should fail
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "" }
        When I DELETE /v1/files
        Then response code should be 500
        Then response body should contain error