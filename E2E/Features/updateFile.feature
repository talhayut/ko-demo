Feature:
As a user of the app
I want to update a file
    
    Scenario: Update a file using existing name and new content
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "taltool.txt", "content": "this is the new file content" }
        When I PUT to /v1/files/taltool.txt
        Then response code should be 200
        Then response body should contain content

    Scenario: Update a file using existing name and no content should fail
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "taltool.txt", "content": "" }
        When I PUT to /v1/files/taltool.txt
        Then response code should be 500
        Then response body should contain error

    Scenario: Update a file using no file name or if it is not a .txt file should fail
        Given I set Content-Type header to application/json
        Given I set Authorization header to eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRhbHRvb2wiLCJpYXQiOjE1MDI5NjYxNjd9.RO8qSNCC82kP95Pav65BPCtGVcJ7ETFTCo9akWSCOV4
        Given I set body to { "name": "", "content": "some content" }
        When I PUT to /v1/files/something
        Then response code should be 500
        Then response body should contain error