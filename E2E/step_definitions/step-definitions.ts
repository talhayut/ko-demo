let apickli = require('apickli');
let fs = require('fs');

module.exports = function () {

    // cleanup before every scenario
    this.Before(function (scenario, callback) {
        const baseUrl = 'localhost:3000';
        this.apickli = new apickli.Apickli('http', baseUrl);
        callback();
    });

    this.Given(/^I set (.*) header to (.*)$/, function (headerName, headerValue, callback) {
        this.apickli.addRequestHeader(headerName, headerValue);
        callback();
    });

    this.Given(/^I set body to (.*)$/, function (body, callback) {
        this.apickli.setRequestBody(body);
        callback();
    });

    this.When(/^I GET (.*)$/, function (resource, callback) {       
        this.apickli.get(resource, function (error) {
            if (error) {
                callback(error);
            }
            callback();
        });
    });

    this.When(/^I PUT to (.*)$/, function (resource, callback) {
        this.apickli.put(resource, function (error) {
            if (error) {
                callback(error);
            }
            callback();
        });
    });

    this.When(/^I POST to (.*)$/, function (resource, callback) {
        this.apickli.post(resource, function (error) {
            if (error) {
                callback(error);
            }
            callback();
        });
    });

    this.When(/^I DELETE (.*)$/, function (resource, callback) {
        this.apickli.delete(resource, function (error) {
            if (error) {
                callback(error);
            }
            callback();
        });
    });


    this.Then(/^response code should be (.*)$/, function (responseCode, callback) {
        let assertion = this.apickli.assertResponseCode(responseCode);
        if (assertion.success) {
            callback();
        }
        else {
            callback(assertion);
        }
    });

    this.Then(/^response body should contain (.*)$/, function (key, callback) {
        let body = this.apickli.getResponseObject().body;
        body = JSON.parse(body);

        if (body.hasOwnProperty(key)) {
            callback();
        } else {
            callback(new Error());
        }
    });

    this.Then(/^response code should equal (.*)$/, function (responseCode, callback) {
        let assertion = this.apickli.assertResponseCode(responseCode.toString());
        if (!assertion.authorized) {
            callback();
        }
        else {
            callback(assertion);
        }
    });

};
